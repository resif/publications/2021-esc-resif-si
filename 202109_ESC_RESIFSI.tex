% Created 2021-09-01 Wed 08:55
% Intended LaTeX compiler: lualatex
\documentclass[aspectratio=169,xcolor=dvipsnames,compress]{beamer}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[english]{babel}
\usepackage[hidelinks]{hyperref}
\usepackage{minted}
\usepackage{lmodern}
\usepackage{fontspec}
\usepackage{sectsty}
\usepackage{xcolor}
\usepackage{booktabs}
\hypersetup{colorlinks,urlcolor=MidnightBlue,linkcolor=,}
\usepackage[type={CC}, modifier={by}, version={4.0}]{doclicense}
\usepackage{fontawesome5}
\usepackage{animate}
\setmonofont{Source Code Pro}
\setmainfont{Latin Modern Sans}
\titlegraphic{\includegraphics[height=.7cm]{resif_logo.png}~~~\includegraphics[height=.7cm]{img/epos.png}\\\vspace{.3cm}{\tiny \url{https://www.resif.fr}\hspace{1cm}\url{https://seismology.resif.fr}\hspace{1cm}\url{sismo-help@resif.fr}\\\vspace{.3cm}\doclicenseImage[imagewidth=5em]}}
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{navigation symbols}{}
\AtBeginSection[]{\begin{frame}\frametitle{}\LARGE\insertsectionhead\vspace{0.1cm}\hrule\end{frame}}
\setbeamertemplate{itemize item}{\faAngleRight}
\setbeamertemplate{itemize subitem}{\faCaretRight}
\usetheme{resif}
\author{Résif Technical Committee}
\date{September 20, 2021}
\title{From raw to FAIR: The Résif-SI data management}
\hypersetup{
 pdfauthor={Résif Technical Committee},
 pdftitle={From raw to FAIR: The Résif-SI data management},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle

\section{Context}
\label{sec:orgd99f272}
\begin{frame}[label={sec:org3c295ef}]{Résif-SI}
\begin{itemize}
\item A \alert{distributed} seismological data management system for France\\
\item 5 organisations collaborating (IPGP, Observatoire Côte d'Azur, École et Observatoire des Sciences de la Terre, Observatoire des Sciences de l'Univers de Grenoble, CEA)\\
\item managing \alert{70 seismological networks} data and metadata\\
\item one unique national endpoint for data and metadata access: Résif-DC\\
\end{itemize}
Fully described by \cite{Péquegnat_2021}.\\
\end{frame}
\begin{frame}[label={sec:org002f137},shrink=20]{FAIR principles}
As described by \cite{Wilkinson_2016}\\
\begin{columns}
\begin{column}[T]{.5\columnwidth}
\begin{block}{Findable}
\begin{itemize}
\item \alert{F1} Globally unique and persistent identifier\\
\item \alert{F2} Data described with rich metadata\\
\item \alert{F3} Metadata identifies clearly the described data\\
\item \alert{F4} (Meta)data indexed and searchable\\
\end{itemize}
\end{block}
\end{column}
\begin{column}[T]{.5\columnwidth}
\begin{block}{Accessible}
\begin{itemize}
\item \alert{A1} (Meta)data retrievable with standard protocol\\
\begin{itemize}
\item \alert{A1.1} open and free protocol\\
\item \alert{A1.2} authentication if necessary\\
\end{itemize}
\item \alert{A2} Metadata survives the data\\
\end{itemize}
\end{block}
\end{column}
\end{columns}
\begin{columns}
\begin{column}[T]{.5\columnwidth}
\begin{block}{Interoperable}
\begin{itemize}
\item \alert{I1} Use formal and well known languages\\
\item \alert{I2} Use vocabularies that follows the FAIR principles\\
\item \alert{I3} Metadata use qualified references to other metadata\\
\end{itemize}
\end{block}
\end{column}
\begin{column}[T]{.5\columnwidth}
\begin{block}{Reachable}
\begin{itemize}
\item \alert{R1} Richly described with a plurality of accurate and relevant attributes\\
\begin{itemize}
\item \alert{R1.1} Clear and open licence\\
\item \alert{R1.2} Detailed provenance\\
\item \alert{R1.3} Meet domain-relevant community standards\\
\end{itemize}
\end{itemize}
\end{block}
\end{column}
\end{columns}
\end{frame}
\section{Data life cycle}
\label{sec:orgfbcc595}
\begin{frame}[label={sec:org3906fa3}]{Overview}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{img/overview1.pdf}
\caption{From \cite{Péquegnat_2021}, a schema of the Résif-SI organisation, emphasizing data workflows and distinct roles for their management.}
\end{figure}
\end{frame}
\begin{frame}[label={sec:org2ae4534}]{Acquisition}
\begin{columns}
\begin{column}{.4\columnwidth}
\begin{center}
\includegraphics[trim=0 0 670 0,clip,width=.9\linewidth]{img/overview1.pdf}
\end{center}
\end{column}
\begin{column}{.6\columnwidth}
\begin{itemize}
\item \alert{Permanent} stations  and \alert{temporary} deployments\\
\item Several transmission protocols:\\
\begin{itemize}
\item Near RealTime seedlink\\
\item Connected stations (ftp, rsync, http)\\
\item Manually retrieved\\
\end{itemize}
\item Collected by an A-Node, holding responsibility for the raw data\\
\end{itemize}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[label={sec:org8738667}]{Validation}
\begin{columns}
\begin{column}{.4\columnwidth}
\begin{center}
\includegraphics[trim=200 0 590 0,clip,scale=.6]{img/overview1.pdf}
\end{center}
\end{column}
\begin{column}{.6\columnwidth}
\begin{itemize}
\item Raw data is validated by A-Node experts\\
\item Agreements on delay for validation vary for each network\\
\item Quality checks are defined by each seismological network\\
\end{itemize}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[label={sec:orgaa63390}]{Submission}
\begin{columns}
\begin{column}{.4\columnwidth}
\begin{center}
\includegraphics[trim=200 0 440 0,clip,width=.9\linewidth]{img/overview1.pdf}
\end{center}
\end{column}
\begin{column}{.6\columnwidth}
\begin{itemize}
\item Each data submission is considered as an atomic transaction.\\
\item For the B-Node, a transaction is an asynchronous step by step list of operations.\\
\item A transaction report is updated in  for the A-Node to be aware of the transaction's progress and status\\
\item If the transaction passes all the quality tests, the data integrates the archive and is made available.\\
\end{itemize}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[label={sec:org1a8b8b2},fragile]{Delivery}
 \begin{columns}
\begin{column}{.4\columnwidth}
\begin{center}
\includegraphics[trim=430 0 160 0,clip,width=.9\linewidth]{img/overview1.pdf}
\end{center}
\end{column}
\begin{column}{.6\columnwidth}
\begin{itemize}
\item B-Node delivers the data mainly through the standard \texttt{fdsnws-dataselect} webservice\\
\item Realtime data is also made available with the seedlink protocol\\
\item Distribution in the FDSN standard \texttt{miniSEEDv2} format\\
\item Data is distributed under the terms of the Creative Common BY open licence.\\
\begin{itemize}
\item[{$\boxtimes$}] A1.1 open and free protocols\\
\item[{$\boxtimes$}] A1.2 (local or EIDA token based authentication)\\
\item[{$\boxtimes$}] A2: Résif-DC manages metadata separately from the data\\
\item[{$\boxtimes$}] I1 Use formal and well known languages\\
\item[{$\boxtimes$}] R1.1 Clear and open licence\\
\end{itemize}
\end{itemize}
\end{column}
\end{columns}
\end{frame}
\section{Metadata management}
\label{sec:org44cf7bd}
\begin{frame}[label={sec:org1328869},fragile]{Metadata production}
 \begin{columns}
\begin{column}{.8\columnwidth}
\begin{itemize}
\item Edition is made by the A-Nodes\\
\item Several tools are used within the Résif-SI organisation\\
\item Normalization of the values in StationXML allows grouping and searching on usefull fields:\\
\begin{itemize}
\item operator\\
\item agencies\\
\end{itemize}
\item As \texttt{Yasmine} hits production, along with \texttt{AROL} \cite{AROL}, there is an official common tool for metadata creation.\\
\end{itemize}
\end{column}

\begin{column}{.2\columnwidth}
\begin{itemize}
\item[{$\boxtimes$}] F2\\
\item[{$\boxtimes$}] F3\\
\item[{$\boxtimes$}] R1.2\\
\item[{$\boxtimes$}] R1.3\\
\end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:orgfeaabb7}]{Submission}
\begin{columns}
\begin{column}{.8\columnwidth}
\begin{itemize}
\item Metadata submission is considered as an atomic transaction\\
\item B-Node applies a list of conformity checks\\
\item Transaction report is updated in real time\\
\item Eventually, the metadata is integrated in the database and accessible to the end user\\
\item B-Node hosts the metadata on safe storage\\
\end{itemize}
\end{column}
\begin{column}{.2\columnwidth}
\begin{itemize}
\item[{$\boxtimes$}] A2\\
\end{itemize}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[label={sec:org39452a5}]{Metadata citation}
\begin{columns}
\begin{column}{.8\columnwidth}
\begin{itemize}
\item B-Node can edit and submit DOI for a network\\
\item Datacite metadata contains the licence (CC-BY), the founders, the PI, and other usefull information to help citation and cross-references\\
\item The StationXML metadata mentions the DOI\\
\item Everything is pulled together to build a rich and usefull landing page for each network\\
\end{itemize}
\end{column}
\begin{column}{.2\columnwidth}
\begin{itemize}
\item[{$\boxtimes$}] F1\\
\item[{$\boxtimes$}] R1.1\\
\item[{$\boxtimes$}] I3\\
\end{itemize}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[label={sec:orge4c6aa9},fragile]{Metadata distribution}
 \begin{columns}
\begin{column}{.8\columnwidth}
\begin{itemize}
\item Through the standard \texttt{fdsnws-station} webservice, providing a rich selection interface\\
\item With the standard StationXML1.1 format\\
\end{itemize}
\end{column}
\begin{column}{.2\columnwidth}
\begin{itemize}
\item[{$\boxtimes$}] F4\\
\item[{$\boxtimes$}] A1.1\\
\end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:orge3d97ea}]{The Résif seismology web portal}
\begin{columns}
\begin{column}{.8\columnwidth}
\url{https://seismology.resif.fr}\\
\begin{itemize}
\item Landing page for seismic networks DOI edited by Résif-DC\\
\item Human friendly presentation of seismology metadata and datacite metadata\\
\item Advanced search features on metadata\\
\begin{itemize}
\item Filters for permanent/temporary, open/closed networks\\
\item Filters on agencies\\
\end{itemize}
\end{itemize}
\end{column}

\begin{column}{.2\columnwidth}
\begin{itemize}
\item[{$\boxtimes$}] F4\\
\end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org8d2f135}]{Screencast}
\href{screencasts/seismology.mp4}{\includegraphics[height=.7\textheight]{screencasts/seismology2/seismology1.png}}\\
\end{frame}
\section*{Conclusion}
\label{sec:org0efeee7}
\begin{frame}[label={sec:org19d3c74},fragile]{Conclusion}
 Résif-SI is an attempt to empower the seismological data into a full featured data management solution, checking almost all the FAIR principles along with full interoperability with EPOS.\\

All FAIR principles are checked, except for the \texttt{I2} for which we still lack a standard thesaurus (work in progress at Résif, EPOS and FDSN)\\
\end{frame}

\section*{Bibliography}
\label{sec:org3dbd6a5}
\begin{frame}[fragile,allowframebreaks,label=]{References}
\printbibliography\\
\bibliographystyle{agu}\\
\bibliography{../../bibliography/references}\\
\end{frame}
\end{document}
